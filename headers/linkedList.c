/*********************************
* Class: MAGSHIMIM Final Project *
* Part A functions			 	 *
**********************************/

#include "linkedList.h"
/*
The linked list look like this:
index:       1				2				3		...
		[pFrame1|p2] -> [pFrame2|p3] -> [pFrame3|p4]  ...
		[Frame1 - Info]		[Frame2 - Info]		[Frame3 - Info]	....
*/
/*
Function will change all frames duration
Input: head of the list, and the new dur.
Output: head
*/
FrameNode* changeAllDuration(FrameNode* head, int newDur)
{
	if (newDur < 0)
	{
		newDur = 1;
	}
	FrameNode* current = head;
	while (current != NULL) //while its not the end of the list
	{
		current->frame->duration = newDur;
		current = current->next;
	}
	return head;
}


/*
Function will change the frame duration by its name.
Input: head of the list, and the frame name and the new dur.
Output: head
*/
FrameNode* changeDuration(FrameNode* head, char frameName[], int newDur)
{
	int flag = 0;
	FrameNode* current = head;
	if (newDur < 0)
	{
		newDur = 1;
	}
	while (current != NULL && flag == 0) //while its not the end of the list or the list is empty and while the flag is false
	{
		if (!(strcmp(current->frame->name, frameName))) //flag == TRUE when we find the frame name in the list.
		{
			flag = 1;
			current->frame->duration = newDur;
		}
		current = current->next;
	}
	if (flag == 0)
	{
		printf("The frame does not exist!\n"); 
	}
	
	return head; 
}

/*
Function will change the placement of one of the frames
input: the head of the list, the name of the frame, the new index
output: the head of the list
*/
FrameNode* changePlacement(FrameNode* head, char name[], int newindex)
{
	FrameNode* frame = NULL;
	FrameNode* copyFrame = NULL;
	frame = nameCheck(head, name);
	if (nameCheck(head, name) == NULL ) // checks if the frame exist
	{
		printf("this frame does not exist\n");
	}
	else
	{
		copyFrame = createFrame(frame->frame->name, frame->frame->duration, frame->frame->path);
		while (newindex > getCount(head))
		{
			printf("The movie contains only %d frames!\nEnter the new index in the movie you wish to place the frame: ", getCount(head));
			scanf("%d", &newindex);
		}
		head = deleteNode(head, name);
		insertAtPositionN(&head, copyFrame, newindex);
	}
	return head;
}



/**
Function will delete a specific frame from the list
Input: the head of the line, name of the frame we want to remove
Output: the head of the list
*/
FrameNode* deleteNode(FrameNode* head, char* name)
{
	FrameNode* curr = head;
	FrameNode* temp = NULL;
	int deleted = 0;
	// if the list is not empty (if list is empty - nothing to delete!)
	if (head)
	{
		// the first node should be deleted?
		if (0 == strcmp(head->frame->name, name))
		{
			deleted = 1; //we gonna delete it...
			head = head->next;
			free(curr);
		}
		else
		{
			while (curr->next)
			{
				if ((0 == strcmp(curr->next->frame->name, name))) // waiting to be on the node BEFORE the one we want to delete
				{
					deleted = 1; //we gonna delete it...
					temp = curr->next; // put aside the node to delete
					curr->next = temp->next; // link the node before it, to the node after it
					free(temp); // delete the node
				}
				else
				{
					curr = curr->next;
				}
			}
		}
	}
	if (deleted == 0) // if we didnt delete the frame its cuz we didnt found it..
	{
		printf("The frame was not found\n");
	}
	return head;
}


/*
Function will add one node to the linked list
input: the head of the list, the info for the new frame
output: the head of the list
*/
FrameNode* addNewFrame(FrameNode* head, char vName[], int vDuration, char vPath[])
{
	FrameNode* newFrame = NULL;
	char name[STR_LEN] = { 0 };
	IplImage* image = cvLoadImage(vPath, 1); //create an image.
	if (!image)
	{
		printf("\nError: Can't find file! Frame will not be added!\n");
	}
	else
	{
		while (nameCheck(head, vName) != NULL)
		{
			printf("The name is already taken, please enter another name\n");
			myFgets(vName, STR_LEN);
		}
		newFrame = createFrame(vName, vDuration, vPath);
		insertAtPositionN(&head, newFrame, getCount(head) + 1);
	}
	
	return head;
}

/*
Function will insert Node into specific place(N)
			NOTE - if you want to insert to the end so its gonna be listlen+1!
input: the head of the list, the new node and the position
output: none
*/
void insertAtPositionN(FrameNode** head, FrameNode* newNode, int position)
{
	int i = 0;
	FrameNode* curr = *head;
	if (position == 1) //position was 1
	{
		if (!*head) // if the list is empty
		{
			*head = newNode;
			newNode->next = NULL;
		}
		else 
		{
			newNode->next = *head;
			*head = newNode;
		}
	}
	else
	{
		for (i = 1; i < position - 1; i++)
		{
			curr = curr->next;
		}
		newNode->next = curr->next; //making the new node to point on the after curr node like:
		// curr - > Node X && newNode -> NULL
		// curr - > Node X && newNode -> Node X
		curr->next = newNode; //making the curr node to point on the new node:
		// curr - > newNode -> Node X
	}
}

/*
Function will counts no. of nodes in linked list
input: the head of the list
output: the number of Frames
*/
int getCount(FrameNode* head)
{
	// Base case 
	if (head == NULL)
		return 0;

	// count is 1 + count of remaining list 
	return 1 + getCount(head->next);
}

/*
Function will create frame for the GIF
input: frame name, frame duration(in ms), frame path(file path)
output: pointer to the new frame
*/
FrameNode* createFrame(char vName[], int vDuration, char vPath[])
{
	FrameNode* myframe = (FrameNode*)malloc(sizeof(FrameNode));
	Frame* info = (Frame*)malloc(sizeof(Frame));
	//////////////////////////////////////////////
	myframe->frame = info;
	strncpy(info->name, vName, STR_LEN);
	strncpy(info->path, vPath, STR_LEN);
	info->duration = vDuration;
	myframe->next = NULL;

	return myframe;
}

/*
Function will search the frame name is already exist
Input: head of the list, and the frame name.
Output: TRUE = 1 \ FALSE = 0
*/
FrameNode* nameCheck(FrameNode* head, char name[])
{
	int flag = 0;
	FrameNode* current = head;
	while (current != NULL && flag == 0) //while its not the end of the list or the list is empty and while the flag is false
	{
		if (!(strcmp(current->frame->name, name))) //flag == TRUE when we find the frame name in the list.
		{
			flag = 1;
		}
		if (flag == 0)
		{
			current = current->next;
		}
	}
	return current;
}