/*********************************
* Class: MAGSHIMIM Final Project *
* Part B functions			 	 *
**********************************/

#include "partB.h"
/*
The function gonna save the project as txt file
Input: head, where the user want to save the project
Output: none

The file gonna look this way:
FrameName]Path]Duration] FrameName]Path]Duration] FrameName]Path]Duration]...
*/
void saveProject(FrameNode* head, char userpath[])
{
	FrameNode* curr = head;
	int dur = 0;
	char Path[STR_LEN + LEN_BIN_AND_NULL] = { 0 };

	strncat(Path, userpath, STR_LEN);
	strncat(Path, ".bin", LEN_BIN_AND_NULL);

	FILE *project = fopen(Path, "wb");

	while (curr) // when curr == NULL, that is the end of the list, and loop will end (NULL is false)
	{
		fwrite(curr->frame->name, sizeof(curr->frame->name), 1, project);
		fwrite(curr->frame->path, sizeof(curr->frame->path), 1, project);
		fwrite(&(curr->frame->duration), sizeof(int), 1, project);
		curr = curr->next;
	}

	fclose(project);
}

FrameNode* openOldProject(FrameNode* head,char oldFilePath[])
{
	char name[STR_LEN] = { 0 }, path[STR_LEN] = { 0 };
	int dur = 0, end = 0 ;
	FILE* oldProjectFile = fopen(oldFilePath, "r");
	//////////////////////////////////////////////
	while (!oldProjectFile) // open file will return NULL if the opening failed
	{
		printf("Error: could not open file! - Make sure u wrote the file path correctly..\nEnter the path again: ");
		myFgets(oldFilePath, STR_LEN);
		oldProjectFile = fopen(oldFilePath, "rb");
	}
	do
	{
		if (ftell(oldProjectFile))
		{
			fseek(oldProjectFile, ftell(oldProjectFile) - sizeof(int), SEEK_SET);
		}
		fread(name, sizeof(name), 1, oldProjectFile);
		fread(path, sizeof(path), 1, oldProjectFile);
		fread(&dur, sizeof(dur), 1, oldProjectFile);
		head = addNewFrame(head, name, dur, path);
	} while (1 == fread(&end, sizeof(int), 1, oldProjectFile)); //while its not the end of the file
	return head;
}

/*
Function will perform the fgets command and also remove the newline
that might be at the end of the string - a known issue with fgets.
input: the buffer to read into, the number of chars to read
*/
void myFgets(char str[], int n)
{
	fgets(str, n, stdin);
	str[strcspn(str, "\n")] = 0;
}
