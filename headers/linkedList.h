#ifndef LINKEDLISTH
#define LINKEDLISTH

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// for the photo check:
#include <opencv2/core/core_c.h>
#include <opencv2/highgui/highgui_c.h>

#define STR_LEN 100
#define FALSE 0
#define TRUE !FALSE

// Frame struct
typedef struct Frame
{
	char name[STR_LEN];
	unsigned int	duration;
	char path[STR_LEN];
} Frame;


// Link (node) struct
typedef struct FrameNode
{
	Frame* frame;
	struct FrameNode* next;
} FrameNode;

FrameNode* addNewFrame(FrameNode* head, char vName[], int vDuration, char vPath[]);
int getCount(FrameNode* head);
void insertAtPositionN(FrameNode** head, FrameNode* newNode, int position);
FrameNode* createFrame(char vName[], int vDuration, char vPath[]);
FrameNode* nameCheck(FrameNode* head, char name[]);
FrameNode* deleteNode(FrameNode* head, char* name);
FrameNode* changePlacement(FrameNode* head, char name[], int newindex);
FrameNode* changeDuration(FrameNode* head, char frameName[], int newDur);
FrameNode* changeAllDuration(FrameNode* head, int newDur);

#endif
