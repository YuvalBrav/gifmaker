#ifndef PARTBH
#define PARTBH

#include "linkedList.h"

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define STR_LEN 100
#define LEN_BIN_AND_NULL 5
#define ASCII_1 93  //]

void saveProject(FrameNode* head, char path[]);
FrameNode* openOldProject(FrameNode* head, char oldFilePath[]);
void myFgets(char str[], int n);

#endif
