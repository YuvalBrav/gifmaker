#include "view.h"
#include "linkedList.h"
#include "partB.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TRUE 1
#define FALSE !TRUE

void printList(FrameNode* head);
void freeList(FrameNode** head);

int main()
{
	FrameNode* newHead = NULL;
	Frame* newNode = NULL;
	char name[STR_LEN] = { 0 }, path[STR_LEN] = { 0 };
	int user_pick = 0, duration = 0, index = 0;
	//////////////////////////////////////////////
	printf("\n!!WELCOME TO GIF MAKER!!\n\n  [0] Create a new project\n  [1] Load existing project\nEnter: ");
	scanf("%d", &user_pick);
	getchar(); // clean buffer
	while (user_pick > 1 || user_pick < 0)
	{
		printf("\ninvalid pick pls try again :D :\n  [0] Create a new project\n  [1] Load existing project\nEnter: ");
		scanf("%d", &user_pick);
		getchar(); // clean buffer
	}
	if (user_pick == 1)
	{
		printf("\nWhere is the project? Enter a full path and file name\n  Example -  C:\\Users\\yuval\\Desktop\\Example.bin\n\nEnter here: ");
		myFgets(path, STR_LEN);
		newHead = openOldProject(newHead, path);
		user_pick = 0; //restart...
	}
	do
	{
		printf("\nWhat would you like to do?\n  [0] Exit\n  [1] Add new Frame\n  [2] Remove a Frame\n  [3] Change frame index\n  [4] Change frame duration\n  [5] Change duration of all frames\n  [6] List frames\n  [7] Play movie!\n  [8] Save project\nEnter you choice here: "); 
		scanf("%d", &user_pick);
		getchar(); // clean buffer
		printf("\n");
		if (user_pick < 0 || user_pick > 8)
		{
			printf("You should type one of the options - 0-8!");
		}
		else
		{
			switch (user_pick)
			{
			case 1:
				printf("*** Creating new frame ***\nPlease insert frame path: ");
				myFgets(path, STR_LEN);
				printf("Please insert frame duration(in miliseconds): ");
				scanf("%d", &duration);
				getchar(); // clean buffer
				printf("Please choose a name for that frame: ");
				myFgets(name, STR_LEN);
				newHead = addNewFrame(newHead, name, duration, path);
				break;
			case 2:
				printf("Enter the name of the frame you wish to erase: ");
				myFgets(name, STR_LEN);
				newHead = deleteNode(newHead, name);
				break;
			case 3:
				printf("Enter the name of the frame: ");
				myFgets(name, STR_LEN);
				printf("Enter the new index in the movie you wish to place the frame: ");
				scanf("%d", &index);
				getchar(); // clean buffer
				newHead = changePlacement(newHead, name, index);
				break;
			case 4:
				printf("Enter the name of the frame: ");
				myFgets(name, STR_LEN);
				printf("Enter the new duration: ");
				scanf("%d", &duration);
				getchar(); // clean buffer
				newHead = changeDuration(newHead, name, duration);
				break;
			case 5:
				printf("Enter the new duration for the frames: ");
				scanf("%d", &duration);
				newHead = changeAllDuration(newHead, duration);
				break;
			case 6:
				if (newHead) //delete..
				{
					printList(newHead);
				}
				break;
			case 7:
				play(newHead);
				break;
			case 8:
				printf("Where to save the project? Enter a full path and file name\n  Example -  C:\\Users\\yuval\\Desktop\\Example\n\nEnter here: ");
				myFgets(path, STR_LEN);
				saveProject(newHead, path);
				break;
			default:
				break;
			}
		}
	} while (user_pick != 0);

	if (newHead)
	{
		freeList(&newHead);
	}
	printf(" (\\     (\\\n");
	printf(" ('\\  _('\\ _\n");
	printf("((/ \\(o\---/o)\n");
	printf("((/  \\( . . )  /|\n");
	printf("((/  _( (T) )-/-l_\n");
	printf(" (/ (  \__ (_/_(__)\n");
	printf("  \\/ \\    `-|   |\n");
	printf("  (   `---.__)--+--|>		BYE!\n");
	printf("   \\    \\  \\ \\  |\n");
	printf("    )    )  ) \\_|\n");
	printf("   (    (  ( \n");
	printf("    \\    )  )`\n");
	printf("     `--^`--^\n");
	getchar();
	return 0;
}

/*
Function will free all memory of a list
input: linked list
output: none
*/
void freeList(FrameNode** head)
{
	Frame* temp = NULL;
	FrameNode* curr = *head;
	while (curr)
	{
		temp = curr->frame;
		curr = (curr)->next;
		free(temp);
	}

	*head = NULL;
}

/*
Function will print a list of frames
input: the head of the list
output: none
*/
void printList(FrameNode* head)
{
	printf("\n=======================================\n  Name		Duration		Path	");
	FrameNode* curr = head;
	while (curr) // when curr == NULL, that is the end of the list, and loop will end (NULL is false)
	{
		printf("\n");
		printf("  %s	   %7d			 %4s", curr->frame->name, curr->frame->duration, curr->frame->path);
		curr = curr->next;
	}
	printf("\n=======================================\n");
}
