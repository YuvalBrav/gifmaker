# GifMaker

This gif maker is wirtten in c and use opencv.

# Getting started

1. Clone the repo:
```
git clone https://gitlab.com/YuvalBrav/gifmaker.git
```
2. Lastly - run the code 
```
prog.c
```

# Introduction
This is a gif maker, you might need to prepare some photos before you run the code.
This code use opencv so you might need to download it.

# Contact
Yuval Braverman - yuvalbr10@gmail.com
